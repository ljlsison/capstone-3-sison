import {useState} from 'react'
import {Container} from 'react-bootstrap'
import {BrowserRouter as Router,Route,Routes} from 'react-router-dom'
import './App.css';

import {UserProvider} from './UserContext';

// Import routes
import AppNavbar from './components/AppNavbar'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Home from './pages/Home'
import Dashboard from './pages/Dashboard'
import Products from './pages/Products'
import ProductView from './components/ProductView'
import Register from './pages/Register'
import ErrorPage from './pages/ErrorPage'
import Orders from './pages/Orders';

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  return (
    <>
    {/* Provides the user context throughout any component inside of it */}
    <UserProvider value = {{user, setUser, unsetUser}}>
    {/* Initializes that dynamic routing will be involved */}
      <Router>
          <AppNavbar/>
          <Container>
            <Routes>
              <Route path ="/" element={<Home/>}/>
              <Route path ="/products" element={<Products/>}/>
              <Route path ="/dashboard" element={<Dashboard/>}/>
              <Route path ="/products/:productID" element={<ProductView/>}/>
              <Route path ="/login" element={<Login/>}/>
              <Route path ="/orders" element={<Orders/>}/>
              <Route path ="/register" element={<Register/>}/>
              <Route path ="/logout" element={<Logout/>}/>
              <Route path ="*" element={<ErrorPage/>}/>
            </Routes>
          </Container>
      </Router>
    </UserProvider>
    </>
  );
}

export default App;
