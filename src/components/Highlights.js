import {Row, Col, Card} from 'react-bootstrap'

export default function Highlights(){
    return(
    <Row className="mt-3 mb-3">
                    <Col xs={12} md={4}>
                        <Card className="cardHighlight p-3">
                            <Card.Body>
                                <Card.Title>
                                    <h2><strong>Quality Products</strong></h2>
                                </Card.Title>
                                <Card.Text>
                                    For every purchase you make, we will ensure there are no damages to each and every item.
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col xs={12} md={4}>
                        <Card className="cardHighlight p-3">
                            <Card.Body>
                                <Card.Title>
                                    <h2><strong>Cash on Delivery!</strong></h2>
                                </Card.Title>
                                <Card.Text>
                                    We offer cash on delivery. We also accept major credit cards
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col xs={12} md={4}>
                        <Card className="cardHighlight p-3">
                            <Card.Body>
                                <Card.Title>
                                    <h2><strong>Free Shipping!</strong></h2>
                                </Card.Title>
                                <Card.Text>
                                    We offer free shipping Nationwide!
                                    *Free shipping for orders above Php 5000!
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
    )
}