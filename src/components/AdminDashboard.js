import {useNavigate } from "react-router-dom";
import { Button} from "react-bootstrap";
import Swal from "sweetalert2";
import EditButton from "./EditButton";

export default function AdminDashboard({products}) {

    const {name, _id, description, price, isActive} = products

    const navigate = useNavigate()

    // Archive an item
    const archiveItem = (productID) => {
        console.log(productID)

        fetch(`${process.env.REACT_APP_API_URL}/products/${productID}/archive`,{
            method: `PATCH`,
            headers: {
                "Content-Type" : "application/json",
                Authorization : `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                isActive: false
            })
        })
        .then(response => response.json())
        .then(result => {
            if(result){                
                Swal.fire({
                    title: "Product Archived!",
                    icon: "success",
                    text: "Product is now archived"
                })

            }
            else {
                Swal.fire({
                    title: "Update Failed!",
                    icon: "error",
                    text: "Something went wrong 😔"
                })
            }
        })
    }
    // Unarchive an item
    const unarchiveItem = (productID) => {
        console.log(productID)

        fetch(`${process.env.REACT_APP_API_URL}/products/${productID}/unarchive`,{
            method: `PATCH`,
            headers: {
                "Content-Type" : "application/json",
                Authorization : `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                isActive: true
            })
        })
        .then(response => response.json())
        .then(result => {
            if(result){                
                Swal.fire({
                    title: "Product Updated!",
                    icon: "success",
                    text: "Product is now unarchived"
                })
            }
            else {
                Swal.fire({
                    title: "Update Failed!",
                    icon: "error",
                    text: "Something went wrong 😔"
                })
            }
        })
    }

    return (
        <>

        <tr>
            <td>{name}</td>
            <td>{_id}</td>
            <td>{description}</td>
            <td>{price}</td>
            {
                isActive?
                <td>Available</td>
                :
                <td>Unavailable</td>
            }
            {
            isActive?
            <td>
            <Button className="m-2" size="sm" variant="danger" onClick={() => archiveItem(_id)}>Archive</Button>
            <EditButton key={products._id} products={products}/></td>
            :
            <td><Button className="m-2" size="sm" variant="success" onClick={() => unarchiveItem(_id)}>Unarchive</Button>
            <EditButton key={products._id} products={products}/></td>                                                             
            }
            
        </tr>
        </>
    );
    }