import { useContext,useEffect, useState} from "react";
import { Container, Card, Button, Row, Col,Modal,Form} from 'react-bootstrap'
import { useParams, useNavigate, Link } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

export default function ProductView(){
    const {productID} = useParams()

    const navigate = useNavigate()

    const {user} = useContext(UserContext)
    const [name, setName] = useState("")
    const [description, setDescription] = useState("")
    const [price, setPrice] = useState(0)
    const [isActive, setIsActive] = useState("")

    const addToCart = (productID) => {

        fetch(`${process.env.REACT_APP_API_URL}/orders/create-order`,{
            method: 'POST',
            headers: {
                "Content-Type":"application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                productID: productID,
                quantity: 1
            })
        })
            .then(response => response.json())
            .then(result => {                
                if(result && !user.isAdmin){                
                Swal.fire({
                    title: "Product Placed!",
                    icon: "success",
                    text: "You have successfully ordered the product"
                })
                navigate(`/products/${productID}`)
            }
            else {
                Swal.fire({
                    title: "Order Failed!",
                    icon: "error",
                    text: "Something went wrong 😔"
                })
            }
            })
    }

    useEffect(()=>{
        fetch(`${process.env.REACT_APP_API_URL}/products/${productID}`)
        .then(response => response.json())
        .then(result => {
            setName(result.name)
            setDescription(result.description)
            setPrice(result.price)
            setIsActive(result.isActive)
        })
    })

return(
    <Container className="mt-5">
        <Row>
            <Col lg={{span:6, offset:3}}>
                <Card>
                    <Card.Body className="text-center">
                        <Card.Title>
                            {name}
                        </Card.Title>
                        <Card.Subtitle>Description:</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                        <Card.Subtitle>Price:</Card.Subtitle>
                        <Card.Text>{price}</Card.Text>
                        <Card.Subtitle>Availability:</Card.Subtitle>
                        {(isActive)?
                        <Card.Text className="text-success"><strong>In Stock</strong></Card.Text>
                        :
                        <Card.Text className="text-danger"><strong>Out of Stock</strong></Card.Text>
                        }
                        {
                            user.id !== null ?                         
                            <Button variant="primary" onClick={() => addToCart(productID)}>Purchase</Button>
                            :
                            <Link className='btn btn-danger btn-block' to="/login">Login to Purchase</Link>                                
                        }
                    </Card.Body>
                </Card>
                <>
                <Link to={'/products'}>Go back</Link>
                </>
            </Col>
        </Row>
    </Container>

)
}