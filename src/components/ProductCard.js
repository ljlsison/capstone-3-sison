import {useState, useEffect} from 'react'
import {Card, Button, Col} from 'react-bootstrap'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

export default function ProductsCard({products}){
    
    // Destructuring the props
    const {name, description, price, _id, isActive} = products

    // Using the state
    
    return(
        <Col xs={12} md={4}>
        <Card className = "product-card my-3">
            <Card.Body>
                <Card.Title className="text-center">{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price</Card.Subtitle>
                <Card.Text>PHP{price}</Card.Text>
                <Card.Subtitle>Availability:</Card.Subtitle>
                {(isActive)?
                <Card.Text className="text-success"><strong>In Stock</strong></Card.Text>
                :
                <Card.Text className="text-danger"><strong>Out of Stock</strong></Card.Text>
                }
                    
            </Card.Body>
            <Card.Footer className='text-center'>
                <Link className='btn btn-primary btn-block w-100' to={`/products/${_id}`}>Details</Link>   
            </Card.Footer>
        </Card>
        </Col>
    )
}

// Prop Types can be used to validate the data coming from the props. You can define each property of the prop and assign specific validation for each of them
ProductsCard.propTypes = {
    products: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
    })
}