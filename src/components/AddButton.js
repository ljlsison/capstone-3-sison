import React, { useState, useEffect } from 'react';
import { Form, Button, Modal} from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function AddButton() {

    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [price, setPrice] = useState('')
    const [isActive, setIsActive] = useState(false)
    const navigate = useNavigate()

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    function addItem(event){
            event.preventDefault()

            fetch(`${process.env.REACT_APP_API_URL}/products/add-product`, {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify({
                    name: name,
                    description: description,
                    price: price
                })
            })
            .then(response => response.json())
            .then(result => {
                if(result === false){
                    Swal.fire({
                        title: "Failed to add product",
                        icon: "error",
                        text: "Something went wrong, it's me, not you"
                    })
                } else {
                    setName('')
                    setDescription('')
                    setPrice('')

                    Swal.fire({
                        title: "Product add success",
                        icon: "success",
                        text:"You have successfully added a product"
                    })
                }
            })
    }

    useEffect(()=>{
        if (name !== "" && description !== "" && price > 0){
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    },[name, description, price])

    return (
    <>
        <Button className="mb-2" variant="primary" size="lg" onClick={handleShow}>
        Add Item
        </Button>

        <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
            <Modal.Title>Add Product Info</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Form onSubmit = {event => addItem(event)}>
                <Form.Group controlId="name">
                    <Form.Label>Product Name</Form.Label>
                    <Form.Control 
                        type="text"
                        placeholder="Enter product name."
                        value = {name}
                        onChange = {event => setName(event.target.value)}
                        required
                        />
                </Form.Group>

                <Form.Group controlId="description">
                    <Form.Label>Product description</Form.Label>
                    <Form.Control 
                        type="textarea"
                        placeholder="Enter product description."
                        value = {description}
                        onChange = {event => setDescription(event.target.value)}
                        required
                        />
                </Form.Group>

                <Form.Group controlId="price">
                    <Form.Label>Product price</Form.Label>
                    <Form.Control 
                        type="number"
                        placeholder="Enter product description."
                        value = {price}
                        onChange = {event => setPrice(event.target.value)}
                        required
                        />
                </Form.Group>                    
            </Form>
        </Modal.Body>
        <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
            Close
            </Button>
            {
                isActive?
                <Button id="submitBtn" variant="primary" onClick={event => addItem(event)}>
                Submit
                </Button>
                :
                <Button id="submitBtn" variant="primary" onClick={event => addItem(event)} disabled>
                Submit
                </Button>
            }
        </Modal.Footer>
        </Modal>
    </>
    );
    }