import { useState,useEffect } from "react";
import { Link } from "react-router-dom";
import PropTypes from 'prop-types'
import Table from 'react-bootstrap/Table';

export default function OrderTable({orders}) {

    const {_id, userID, productID,totalAmount, purchasedOn } = orders
    return (
        <tr>
            <td>{_id}</td>
            <td>{userID}</td>
            <td><Link to={`/products/${productID}`}>{productID}</Link></td>
            <td>{totalAmount}</td>
            <td>{purchasedOn}</td>
        </tr>
    );
    }

// OrderTable.prototype = {
//     orders: PropTypes.shape({
//         _id: PropTypes.string.isRequired,
//         userID: PropTypes.string.isRequired,
//         productID: PropTypes.string.isRequired,
//         totalAmount: PropTypes.number.isRequired,
//     })
// }