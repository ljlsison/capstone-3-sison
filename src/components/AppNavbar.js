import {Nav, Navbar} from 'react-bootstrap'
import {Link, NavLink} from 'react-router-dom'
import {useContext} from 'react'
import UserContext from '../UserContext'
import logo_transparent from '../images/logo_transparent.png'

export default function AppNavbar() {

    const {user} = useContext(UserContext)

    return (
    <Navbar bg="warning" variant="dark" expand="lg" sticky="top">
        <Navbar.Brand className="brand" as={Link} to="/"><img src={logo_transparent} height ="50"alt ="CYNCON" /></Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav" className='right-aligned'>
            <Nav className="navbar ml-auto">
                <Nav.Link as={NavLink} to="/">Home</Nav.Link>
                {   (user.isAdmin) ?
                    <Nav.Link as={NavLink} to="/dashboard">Dashboard</Nav.Link>
                    :
                    <Nav.Link as={NavLink} to="/products">Products</Nav.Link>
                }
                
                {   (user.id && user.isAdmin) ?
                        <>
                        <Nav.Link as={NavLink} to="/orders">Orders</Nav.Link>
                        <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                        </>
                        :
                        (user.id && !user.isAdmin)?
                            <>
                            <Nav.Link as={NavLink} to="/orders">My Orders</Nav.Link>
                            <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                            </>
                        :
                        <>
                        <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                        <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
                    </>
                }
            </Nav>
        </Navbar.Collapse>
    </Navbar>
    );
}