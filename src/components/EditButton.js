import React, { useState, useEffect } from 'react';
import { Form, Button, Modal, } from 'react-bootstrap';
import { useNavigate, } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function EditButton({products}) {

    const {name, _id, description, price} = products

    const navigate = useNavigate()

    const [productName, setProductName] = useState(name)
    const [productDescription, setProductDescription] = useState(description)
    const [productPrice,  setProductPrice] = useState(price)
    const [isActive, setIsActive] = useState(false)

    
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    function editItem(event){
            event.preventDefault()

            fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/update`, {
                method: 'PATCH',
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify({
                    name: productName,
                    description: productDescription,
                    price: productPrice
                })
            })
            .then(response => response.json())
            .then(result => {
                if(result === false){
                    Swal.fire({
                        title: "Failed to update product",
                        icon: "error",
                        text: "Something went wrong, it's me, not you"
                    })
                } else {
                    setProductName('')
                    setProductDescription('')
                    setProductPrice('')

                    Swal.fire({
                        title: "Success",
                        icon: "success",
                        text:"You have successfully edited a product"
                    })
                    
                    navigate('/products')
                }
            })
    }

    useEffect(()=>{
        if (productName !== "" && productDescription !== "" && productPrice > 0){
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    },[productName, productDescription, productPrice])

    return (
    <>
        <Button className="mx-2" variant="primary" size="sm" onClick={handleShow}>
        Edit
        </Button>

        <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
            <Modal.Title>Edit Product Info</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Form onSubmit = {event => editItem(event)}>
                <Form.Group controlId="name">
                    <Form.Label>Product Name</Form.Label>
                    <Form.Control 
                        type="text"
                        placeholder="Enter product name."
                        onChange = {event => setProductName(event.target.value)}
                        required
                        />
                </Form.Group>

                <Form.Group controlId="description">
                    <Form.Label>Product description</Form.Label>
                    <Form.Control 
                        type="textarea"
                        placeholder="Enter product description."
                        onChange = {event => setProductDescription(event.target.value)}
                        required
                        />
                </Form.Group>

                <Form.Group controlId="price">
                    <Form.Label>Product price</Form.Label>
                    <Form.Control 
                        type="number"
                        placeholder="Enter product description."
                        onChange = {event => setProductPrice(event.target.value)}
                        required
                        />
                </Form.Group>                    
            </Form>
        </Modal.Body>
        <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
            Close
            </Button>
            {
                isActive?
                <Button id="submitBtn" variant="primary" onClick={event => editItem(event)}>
                Submit
                </Button>
                :
                <Button id="submitBtn" variant="primary" onClick={event => editItem(event)} disabled>
                Submit
                </Button>
            }
        </Modal.Footer>
        </Modal>
    </>
    );
    }