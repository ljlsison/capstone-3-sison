import { Row, Col} from 'react-bootstrap'
import { Link } from 'react-router-dom'
import UserContext from '../UserContext'
import { useContext } from 'react'
import catcart from '../images/catcart.png'
export default function Banner(){

    const {user} = useContext(UserContext)

    return(
        <Row>
            <Col className="Banner p-3">
                <h1><b>CYNCON PET SUPPLIES</b></h1>
                
                <img src={catcart} alt='cat with cart'/>
                <h3><b>Your One Stop Shop for your pet needs!</b></h3>
                {
                    user.isAdmin?
                    <Link className='btn btn-danger' to='/dashboard'>Dashboards</Link>
                    :
                    <Link className='btn btn-danger' to='/products'>Shop now!</Link>
                }
            </Col>
        </Row>   
    )
}