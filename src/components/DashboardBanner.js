import AddButton from './AddButton'

export default function Banner(){
    return(
        <>
            <h1 className='text-align-center'>Admin Dashboard</h1>
            <AddButton/>
        </>
    )
}