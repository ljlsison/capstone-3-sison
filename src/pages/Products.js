import { useEffect,useState, useContext } from "react";
import { useNavigate } from "react-router-dom";
import ProductCard from "../components/ProductCard";
import Loading from "../components/Loading";
import UserContext from "../UserContext";
import {Row, Col} from 'react-bootstrap'
// import courses_data from "../data/courses"; No needed anymore

export default function Products(){
    const {user, setUser} = useContext(UserContext)

    const navigate = useNavigate()

    const [products,setProducts] = useState([])
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        // Sets the loading state to true
        setLoading(true)
        if(user.isAdmin){
            navigate(`/dashboard`)
        }
        if(!user.isAdmin){
            fetch(`${process.env.REACT_APP_API_URL}/products`)
            .then(response => response.json())
            .then(result => {
                setProducts(
                    result.map(products => {
                        return (
                            <ProductCard key={products._id} products={products}/>
                            )
                        })
                    )
                setLoading(false)
            })
        }
    },[])

    return(
            (loading)?
            <Loading/>
            :
        <>
        <Row className="mt-3 mb-3">
            {products}
        </Row>
        </>
    ) 
}