import {Navigate} from 'react-router-dom'
import UserContext from '../UserContext'
import {useContext, useEffect} from 'react'
import Swal from 'sweetalert2'

export default function Logout(){
    const {unsetUser, setUser} = useContext(UserContext)

    // Using the context, clear the contents of the localStorage
    unsetUser()

    // An effect which removes the user mail from the global user state that comes from context
    useEffect(()=> {
        setUser({
            id:null
        })
    },[])

    Swal.fire('Iiwan mo nanaman aq 😭')


return(
    <Navigate to="/login"/>
)
}