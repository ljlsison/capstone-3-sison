import {Form, Button, Card} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react'
import {useNavigate, Navigate, Link} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Register(){

    const {user, setUser} = useContext(UserContext)

    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [email, setEmail] = useState('')
    const [mobileNo, setMobileNo] = useState('')
    const [password1, setPassword1] = useState('')
    const [password2, setPassword2] = useState('')

    const navigate = useNavigate()

    // for determining if button is disabled or not
    const [isActive, setIsActive] = useState(false)

    function registerUser(event){
        event.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/users/check-email`,{
            method: 'POST',
            headers: {
                'Content-Type':'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        }).then(response => response.json())
        .then(result => {
            if(result === true){
                Swal.fire({
                    title: 'Oops!',
                    icon: 'error',
                    text: 'Email is already in use, try a different one.'
                })
            }
            else {
                fetch(`${process.env.REACT_APP_API_URL}/users/register`,{
                    method: 'POST',
                    headers: {
                        'Content-Type':'application/json'
                    },
                    body: JSON.stringify({
                            firstName: firstName,
                            lastName: lastName,
                            email: email,
                            password: password1,
                            mobileNo: mobileNo
                        })
                })
                .then(response => response.json())
                .then(result => {
                    if(result === false){
                        Swal.fire({
                            title: 'Registration Failed!',
                            icon: 'error',
                            text: 'Something went wrong, try again later'

                        })
                    } else {

                        setFirstName('')
						setLastName('')
						setMobileNo('')
						setEmail('')
						setPassword1('')
						setPassword2('')

                        Swal.fire({
                            title: 'Registration Success!',
                            icon: 'success',
                            text: 'Welcome to our Shop!'
                        })

                    navigate("/login")
                    }
                })
            }
        })
    }

    useEffect(() => {
        if(
            (
                firstName !== '' && 
                lastName !== '' && 
                email !== '' && 
                mobileNo.length === 11 &&
                password1 !== '' && 
                password2 !== ''
            ) 
        && (password1 === password2)
        ){
            // Enables the submit button if the from data has been verified
            setIsActive(true)
        } else {
            setIsActive(false)
        }

    }, [firstName, lastName, email, mobileNo, password1, password2])

    return(
        (user.id !== null) ?
        <Navigate to="/courses"/>
        :
        <Card className="login my-3 text-left w-50 m-auto">
        <Card.Header className="mt-2"><h1>Register</h1></Card.Header>
        <Card.Body>

        <Form onSubmit = {event => registerUser(event)}>
            <Form.Group controlId="firstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Enter your first name."
                    value = {firstName}
                    onChange = {event => setFirstName(event.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="lastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Enter your last name."
                    value = {lastName}
                    onChange = {event => setLastName(event.target.value)}
                    required
                />
            </Form.Group>            

            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value = {email}
                    onChange = {event => setEmail(event.target.value)}
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="mobileNo">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control 
                    type="string" 
                    placeholder="Enter your mobile number."
                    value = {mobileNo}
                    onChange = {event => setMobileNo(event.target.value)}
                    required
                />
                <Form.Text className="text-muted">
                    Example: 09123456789
                </Form.Text>
            </Form.Group>   

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value = {password1}
                    onChange = {event => setPassword1(event.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Verify Password"
                    value = {password2}
                    onChange = {event => setPassword2(event.target.value)}
                    required
                />
            </Form.Group>

            {
                isActive ? 
                <Button className="my-3" variant="primary" type="submit" id="submitBtn">
                Submit
                </Button>
                :
                <Button className="my-3" variant="primary" type="submit" id="submitBtn" disabled>
                Submit
                </Button>
            }
        </Form>  
        </Card.Body>
        <p className="text-center">Already a user? <Link to="/login">Click here</Link> to log in</p>
    </Card>
    )
}