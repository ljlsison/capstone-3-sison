import {Link} from 'react-router-dom'

export default function ErrorPage(){
    return (
        <>
        <h1>Oops...</h1>
        <p>Page does not exist :(</p>
        <Link to="/">Go Back Home</Link>
        </>
    )
}