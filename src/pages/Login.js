import {Form, Button, Card} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react'
import {useNavigate, Navigate, Link} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Login(){
    // Initializes the use of the properties from the UserProvider in App.js file
    const {user, setUser} = useContext(UserContext)
    
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const [isActive, setIsActive] = useState(false)
    const navigate = useNavigate()

    const retrieveUser = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(response => response.json())
        .then(result => {
            console.log(result)

            // Store the user details retrieved from the token into the global user state
            setUser({
                id: result._id,
                isAdmin: result.isAdmin
            })
        })
    }

    function authenticate(event){
        event.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type':'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(response => response.json())
        .then(result => {
            if(typeof result.accessToken !== "undefined"){
                localStorage.setItem('token', result.accessToken)

                retrieveUser(result.accessToken)

                Swal.fire({
                    title: 'Login Successful!',
                    icon: 'success',
                    text: 'Welcome to our Store!'
                })

                navigate("/")
            } else {
                Swal.fire({
                    title: 'Login Failed!',
                    icon: 'error',
                    text: 'There\'s something wrong, it\'s you not me 👉👈'
                })
            }
        })

    }


    // monitors state 
    useEffect(() => {
        if(email !== '' && password !== ''){
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    // states to be monitored
    },[email,password])

    return(
        (user.id !== null)?
            <Navigate to="/courses"/>
            :  
            <Card className="login my-3 text-left  w-50 m-auto">
            <Card.Header className="mt-2"><h1>Log in</h1></Card.Header>
            <Card.Body>
            <Form onSubmit = {event => authenticate(event)}>
                <Form.Group controlId="userEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control 
                        type="email" 
                        placeholder="Enter email"
                        value = {email}
                        onChange = {event => setEmail(event.target.value)}
                        required
                    />
                    <Form.Text className="text-muted">
                        We'll never share your email with anyone else.
                    </Form.Text>
                </Form.Group>

                <Form.Group controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Password"
                        value = {password}
                        onChange = {event => setPassword(event.target.value)}
                        required
                    />
                </Form.Group>
                {
                    isActive ? 
                    <Button className="mt-2" variant="primary" type="submit" id="submitBtn">
                    Submit
                    </Button>
                    :
                    <Button className="mt-2" variant="primary" type="submit" id="submitBtn" disabled>
                    Submit
                    </Button>
                }      
            </Form>
            </Card.Body>
            <p className="text-center">Don't have an account? <Link to="/register">Click here</Link> to register</p>
            </Card>
    )
}