import { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import OrderTable from "../components/OrderTable";
import UserContext from "../UserContext";
import { Table } from "react-bootstrap";


export default function Orders(){
    const {user, setUser} = useContext(UserContext)

    const navigate = useNavigate()

    const [orders, setOrders] = useState([])
    
    useEffect(() => {
        if(user.isAdmin){
            fetch(`${process.env.REACT_APP_API_URL}/orders/all-orders`,{
                headers: {
                    method: 'GET',
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                }
            })
            .then(response => response.json())
            .then(result => {
                console.log(result)
                setOrders(
                    result.map(orders => {
                        return (
                            <OrderTable key={orders._id} orders={orders}/>
                            )
                        })
                    )
            })
        }
        if (!user.isAdmin){
            fetch(`${process.env.REACT_APP_API_URL}/orders/my-orders`,{
                headers: {
                    method: 'GET',
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                }
            })
            .then(response => response.json())
            .then(result => {
                console.log(result)
                setOrders(
                    result.map(orders => {
                        return (
                            <OrderTable key={orders._id} orders={orders}/>
                            )
                        })
                    )
            })
        }
    },[])
    
    return (
        <>
        <Table striped bordered hover variant="dark">
        <thead>
        <tr>
            <th>OrderID</th>
            <th>UserID</th>
            <th>ProductID</th>
            <th>Total Amount</th>
            <th>Date of Purchase</th>
        </tr>
        </thead>
        <tbody>
        {orders}
        </tbody>
        </Table>
        </>
        )
}