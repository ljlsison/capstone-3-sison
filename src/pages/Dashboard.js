import { useEffect,useState, useContext } from "react";
import { useNavigate } from "react-router-dom";
import { Table } from "react-bootstrap";
import Loading from "../components/Loading";
import UserContext from "../UserContext";
import DashboardBanner from "../components/DashboardBanner"
import AdminDashboard from "../components/AdminDashboard";
// import courses_data from "../data/courses"; No needed anymore

export default function Products(){
    const {user} = useContext(UserContext)

    const navigate = useNavigate()

    const [products,setProducts] = useState([])
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        // Sets the loading state to true
        if(!user.isAdmin){
            navigate(`/products`)
        }
        if(user.isAdmin){
            fetch(`${process.env.REACT_APP_API_URL}/products/all`)
            .then(response => response.json())
            .then(result => {
                setProducts(
                    result.map(products => {
                        return (
                            <AdminDashboard key={products._id} products={products}/>
                            )
                        })
                    )
            })
        }
    },[products])

    return(
            (loading)?
            <Loading/>
            :
        <>
        <DashboardBanner/>
        <Table striped bordered hover variant="dark">
        <thead>
            <tr>
            <th>Name</th>
            <th>Product ID</th>
            <th>Description</th>
            <th>Price</th>
            <th>Availability</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
            {products}
        </tbody>
        </Table>
        </>
    ) 
}